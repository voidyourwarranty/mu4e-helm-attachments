# mu4e-helm-attachments : Helm based functions to work with attachments for the Emacs email system mu4e

This is an Emacs Lisp project that adds functions to work with attachments to the email program
[mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) by Dirk-Jan C. Binnema. The new functions make use of
[helm](https://github.com/emacs-helm/helm) by Thierry Volpiatto.

# Features

## Saving, viewing and opening attachments.

The function `ma-helm-menu` is called in the message view mode of `mu4e` and offers a helm based selection of
attachments of the current message. The default action saves the selected attachments. It is also possible to view the
selected attachments using programs based on the MIME type of the attachments or using programs that the user specifies
for each selected attachment.

The function `ma-helm-view` is called in the message view mode of `mu4e` and merely opens the selected attachments using
programs based on the MIME type of the attachments.

The MIME types `text/plain` and `text/x-org` are configured to be opened in Emacs by calling `emacsclient`. Further MIME
types can be configured by the user by adding to the alist `ma-view-commands` as shown in the configuration example
below.

If the MIME type of an attachment is not found in `ma-view-commands` we call the command `xdg-mime` as a fall-back. If
that fails, we open the attachment by calling `run-mailcap`.

Both fall-backs can be disabled by setting `(setq ma-xdgmime nil)` or `(setq ma-runmailcap nil)`, respectively. If not
`nil`, these variables refer to the respective executables. If you know you don't have them installed, you may wish to
set the variables to `nil` in order to avoid spurious messages to be logged in the `*Messages*` buffer.

## Security Advice

Automatically opening email attachments or running shell commands on them poses well-known security risks. You need to
take into account that at least decrypted versions of the attachments may get written to temporary files. So if you work
with crucial encrypted emails, you need to secure your temporary file storage as well.

What happens to an attachment is determined, in this order, by
1. the MIME type that identifies the attachment in the current email (this MIME type is diplayed in the helm selection - it was chosen by the sender of the email, so it may be fake) and by whatever your `ma-view-commands` says,
2. whatever `xdg-mime` suggests for this MIME type,
3. whatever `run-mailcap` does to that attachment given that MIME type.

In order to be on the safe side, you may need to use a very small `ma-view-commands` with carefully selected commands
only, and in addition disable the two fall-backs.

# Configuration

In order to load `mu4e-helm-attachments`, you place the file `mu4e-helm-attachments.el` in the
load-path and
```
(require 'mu4e-helm-attachments)
```

An example configuration with suggested key bindings is as follows,
```
;; The helm browsing of attachments is bound to [e] in view-mode, replacing the old save attachments function.
(define-key mu4e-view-mode-map "e" 'ma-helm-menu)

;; The helm selection of attachments to view is bound to [V] in view-mode.
(define-key mu4e-view-mode-map "V" 'ma-helm-view)

;; Specify the default command to open an attachment.
(setq ma-default-command "xdg-open")

;; We specify the executables to open the various MIME types with.
(setq ma-view-commands (append ma-view-commands
  '(("application/pdf"          . "okular")
    ("application/postscript"   . "okular")
    ("image/bmp"                . "okular")
    ("image/gif"                . "okular")
    ("image/jpeg"               . "okular")
    ("image/png"                . "okular")
    ("image/tiff"               . "okular")
    ("application/msexel"       . "libreoffice")
    ("application/mspowerpoint" . "libreoffice")
    ("application/msword"       . "libreoffice"))))
```

# Internals

## Name space

All variables and functions defined by `mu4e-helm-attachments` use the prefix `ma-`.

## MIME types

Note that there are weird email clients out there, primarily in the Windows world, that provide misleading MIME types
for their attachments. I do not know any good may of dealing with it.

## Tasks and Suggestions

- issue error messages when an executable is missing
- add an action to always open the attachment in Emacs (`find-file`)
- add an action to pipe the attachment into a Unix command
- add an action to add a vCard `.vcs`attachment to the contacts data base
- add an action to add an iCal attachment to the agenda schedule

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
