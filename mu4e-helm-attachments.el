;;; -*- mode:emacs-lisp -*-
;;; ================================================================================
;;; File:    mu4e-helm-attachments.el
;;; Date:    2020-10-11
;;; Author:  Void Your Warranty
;;; License: Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
;;; Purpose: Attachment functions for mu4e using helm.
;;; ================================================================================

(require 'mu4e)
(require 'org-mu4e)

(require 'helm)
(require 'helm-config)

(server-start)

(defcustom ma-xdgmime "xdg-mime"
  "The name of the xdg-mime executable, to be found anywhere in the exec-path.")

(defcustom ma-runmailcap "run-mailcap"
  "The name of the run-mailcap executable, to be found anywhere in the exec-path.")

(defcustom ma-view-commands
  '(("text/plain"             . "emacsclient")
    ("text/x-org"             . "emacsclient"))
  "The association of MIME types with executables to open the attachment with.")

(defun ma-attachment-selection (msg)
  "For a message <msg> obtain an alist (\"string\" . structure)
  where the \"string\" specifies an attachment and the structure
  is the m4e-message-part that describes the MIME part of the
  message that is the corresponding attachment."
  (interactive)
  (let* ((id 0)
	 (partcount (length (mu4e-message-field msg :parts)))
	 (attachments
  ;; we only list parts that look like attachments, ie. that have a
  ;; non-nil :attachment property; we record a mapping between
  ;; user-visible numbers and the part indices
	  (cl-remove-if-not
	   (lambda (part)
	     (let* ((mtype (or (mu4e-message-part-field part :mime-type)
			       "application/octet-stream"))
		    (partsize (or (mu4e-message-part-field part :size) 0))
		    (attachtype (mu4e-message-part-field part :type))
		    (isattach
		     (or ;; we consider parts marked either
		      ;; "attachment" or "inline" as attachment.
		      (member 'attachment attachtype)
		      ;; list inline parts as attachment (so they can be
		      ;; saved), unless they are text/plain, which are
		      ;; usually just message footers in mailing lists
		      ;;
		      ;; however, slow bigger text parts as attachments,
		      ;; except when they're the only part... it's
		      ;; complicated.
		      (and (member 'inline attachtype)
			   (or
			    (and (> partcount 1) (> partsize 256))
			    (not (string-match "^text/plain" mtype)))))))
	       (or ;; remove if it's not an attach *or* if it's an
		;; image/audio/application type (but not a signature)
		isattach
		(string-match "^\\(image\\|audio\\)" mtype)
		(string= "message/rfc822" mtype)
		(string= "text/calendar" mtype)
		(and (string-match "^application" mtype)
		     (not (string-match "signature" mtype))))))
	   (mu4e-message-field msg :parts))))
    (setq i 0)
    (mapcar (lambda (part)
	      (list (format "[%d] - %s - %d bytes (%s) - index %d"
			    (setq i (+ i 1))
			    (mu4e-message-part-field part :name)
			    (mu4e-message-part-field part :size)
			    (mu4e-message-part-field part :mime-type)
			    (mu4e-message-part-field part :index)) (plist-put part :attachno i)))
			    attachments)))

(defun ma-attachment-save (candidate)
  "Helm action to save attachments."
  (dolist (i (helm-marked-candidates))
    (mu4e-view-save-attachment-single nil (plist-get (car i) :attachno))))

(defun ma-view-open-attachment-single (attachnum mime)
  "Open a single attachment with an application selected depending on the mime type."
  (let* ((program (cdr (assoc mime ma-view-commands)))
	(executable (if program (locate-file program exec-path exec-suffixes 1) nil)))
    (when program
      (if executable
	  (if (file-executable-p executable)
	      ;; open the attachment with the specified command if any
	      (mu4e-view-open-attachment-with (mu4e-message-at-point) attachnum executable)
	    (error "The list 'ma-view-commands' specifies a command '%s', but it is not execuable." program))
	(error "The list 'ma-view-commands' specifies a command '%s', but it cannot be found in the 'exec-path'." program)))

    (unless program
      ;; If no command has been specified, we first write the attachment to a temporary file.
      (let* ((msg (mu4e-message-at-point))
	     (att (mu4e~view-get-attach msg attachnum))
	     (fpath (make-temp-file "mu4e-helm-attachments-"))
	     (index (plist-get att :index)))

	(mu4e~proc-extract 'save (mu4e-message-field msg :docid) index mu4e-decryption-policy fpath)

	;; We then try to find and execute the program <ma-xdgmime> in order to suggest an executable to open the attachment with.
	(when ma-xdgmime
	  (let ((thexdgmime (locate-file ma-xdgmime exec-path exec-suffixes 1)))
	    (if thexdgmime
		(if (file-executable-p thexdgmime)
		    (let* ((command (concat thexdgmime " query default " mime))
			   (result  (shell-command-to-string command)))
		    (if result
			(if (string-match "\\(.*\\)\\.desktop\n$" result)
			    (setq program (match-string 1 result))
			  (message (format "[mu4e-helm-attachments] Calling '%s' did not result in '<command>.desktop\\n', but rather in '%s'." command result)))
		      (message (format "[mu4e-helm-attachments] Calling '%s' failed." command))))
		  (message (format "[mu4e-helm-attachments] The variable 'ma-xdgmime' specifies a command '%s', but it is not executable." ma-xdgmime)))
	      (message (format "[mu4e-helm-attachments] The variable 'ma-xdgmime' specifies a command '%s', but it cannot be found in the 'exec-path'." ma-xdgmime)))))

	(when program
	  (let ((executable (locate-file program exec-path exec-suffixes 1)))
	    (if executable
		(if (file-executable-p executable)
		    (mu4e-view-open-attachment-with (mu4e-message-at-point) attachnum executable)
		  (progn
		    (message (format "[mu4e-helm-attachments] The program '%s' has suggested a command '%s', but it is not executable." ma-xdgmime program))
		    (setq program nil)))
	      (progn
		(message (format "[mu4e-helm-attachments] The program '%s' has suggested a command '%s', but it cannot be found in the 'exec-path'." ma-xdgmime program))
		(setq program nil)))))

	(unless program

	  ;; We finally try to find an execute the program <ma-runmailcap> in order to suggest an executable to open the attachment with.
	  (when ma-runmailcap
	      (let ((therunmailcap (locate-file ma-runmailcap exec-path exec-suffixes 1)))
		(if therunmailcap
		    (if (file-executable-p therunmailcap)
			(progn
			  (shell-command (concat therunmailcap " " mime ":" fpath))
			  (setq program t))
		      (message (format "[mu4e-helm-attachments] The variable 'ma-runmailcap' specifies a command '%s', but it is not executable." ma-runmailcap)))
		  (message (format "[mu4e-helm-attachments] The variable 'ma-xdgmime' specifies a command '%s', but it cannot be found in the 'exec-path'." ma-runmailcap))))))

	(unless program
	  (error "Unknown MIME type '%s'. Consider adding a suitable command to 'ma-view-commands'." mime))))))

(defun ma-view-open-attachment-single-with (attachnum)
  "Open a single attachment with a user specified application."
  (mu4e-view-open-attachment-with (mu4e-message-at-point) attachnum))

(defun ma-attachment-open (canddate)
  "Helm action to open attachments."
  (dolist (i (helm-marked-candidates))
    (ma-view-open-attachment-single (plist-get (car i) :attachno) (plist-get (car i) :mime-type))))

(defun ma-attachment-open-with (canddate)
  "Helm action to open attachments with a user specified application."
  (dolist (i (helm-marked-candidates))
    (ma-view-open-attachment-single-with (plist-get (car i) :attachno))))

(defun ma-helm-menu ()
  "Helm menu to work with email attachments."
  (interactive)
  (helm :sources (helm-build-sync-source "Select Email Attachments (Save, View)"
		   :candidates (ma-attachment-selection (mu4e-message-at-point))
                   :action #'(("Save Attachment" . ma-attachment-save)
			      ("View Attachment" . ma-attachment-open)
			      ("Open Attachment" . ma-attachment-open-with)))
	:buffer "*helm attachments menu*"))

(defun ma-helm-view ()
  "Helm selection of email attachments to view."
  (interactive)
  (helm :sources (helm-build-sync-source "Select Email Attachments (Save, View)"
		   :candidates (ma-attachment-selection (mu4e-message-at-point))
                   :action #'ma-attachment-open)
	:buffer "*helm attachments view*"))

(provide 'mu4e-helm-attachments)

;;; ================================================================================
;;; End of file.
;;; ================================================================================
